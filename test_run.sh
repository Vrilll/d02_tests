#!/bin/bash

if [ -z "$1" ]
then
	echo "usage: ./test_run.sh file_to_run.c"
	echo "only for exercises that use ft_printchar"
else
	/usr/bin/norminette -R CheckForbiddenSourceHeader $1
	FUNCTIONNAME=$(basename -s ".c" $1)
	sed "s/function_name/$FUNCTIONNAME/g" tests/main.c > tests/main_function.c
	gcc -Wextra -Wall -Werror tests/main_function.c tests/ft_putchar.c $1 -o tmp_exec
	./tmp_exec
	rm tmp_exec tests/main_function.c
	echo
	echo "Run finished."
fi

