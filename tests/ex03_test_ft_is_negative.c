/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test_ft_is_negative.c                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/08/08 16:59:53 by tjans         #+#    #+#                 */
/*   Updated: 2019/08/08 17:04:23 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

void	ft_is_negative(int n);

int		main(void)
{
	ft_is_negative(-5);
	ft_is_negative(0);
	ft_is_negative(5);
}
